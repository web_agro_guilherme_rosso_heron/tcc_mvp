<?php	
@session_start();
if(isset($_SESSION['logado'])){
$logado=$_SESSION['logado'];
include_once("funcoes.php");
}else {
   $logado=0;
}
if($logado==0){
      header("Location: index.php?id=erro_login");
}else {
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
    <meta http-equiv=”content-type” content="text/html;" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="keywords" content="tags, que, eu, quiser, usar, para, os, robos, do, google" />
    <title> AgroWeb</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<!-- BOOTSTRAP -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
        
		<!-- ESTILOS PARA ESTA PÁGINA -->
		<!-- Nesse caso, este estilo é apenas para inserir imagens -->
		<link rel="shortcut icon" href="imagens/favicon.png" type="image/x-icon" />
		<!-- JAVASCRIPT E JQUERY -->
 <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>

</head>
<script src="bootstrap/js/jquery-3.3.1.min.js"></script>
<body>
</head>

<?php
$PDO = conectar();
$sql = "SELECT *  FROM mensagens where lido='n'";
$pesquisa= $PDO->prepare($sql);
$pesquisa->execute();
echo "<br><br>";
echo "<br><br>";
?>

	<br>
	<div class="container">
	<div class="table-responsive">
	<table class="table table-hover table-bordered">
   <thead>
   <tr>
      <th scope="col">Nome</th>
      <th scope="col">Email</th>
      <th scope="col">Fone</th>
      <th scope="col">Mensagem</th>
	  <th scope="col">Lido</th>
    </tr>
  </thead>
  <tbody>
<?php
	while($resultado = $pesquisa->fetch(PDO::FETCH_ASSOC)){
$cod=$resultado['cod'];
$res=$resultado['resposta'];
if($res=="n"){
$msg="<p align=center><a href=index.php?id=responder_mensagem&cod=$cod>Responder";
        }else {
  $msg="<p align=center><font color=red>Respondido</font>";
}
?>
		
    <tr>
      <th scope="row"><?php echo  utf8_encode($resultado['nome']);?></th>
      <td><?php echo $resultado['email'],"$msg"; ?></td>
      <?php
$telefone = $resultado['fone'];
$pattern = '/(\d{2})(\d{4})(\d*)/';
$telefoneN = preg_replace($pattern, '($1) $2-$3', $telefone);
      ?><td><?php echo $telefoneN; ?></td>
      <td><?php echo $resultado['mensagem'];?></td>
	  <?php
	  if($resultado['lido']=="n"){
	  ?>
	  <td><?php echo "<a href=index.php?id=conf_altdel&cod=$cod&&acao=3>Marcar como lido</a>";?></td>
    </tr>
	
<?php  
	}
	}
	?>
	</tbody>
	</table>
	</div>
	</div>
<?php
	}
	?>