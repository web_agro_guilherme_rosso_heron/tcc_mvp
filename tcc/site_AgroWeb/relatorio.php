<?php

@session_start();
if(!isset($_SESSION['logado'])){
  header("Location: index.php?id=erro_login")	;
  exit;
}else {
	$conexao= new PDO ('mysql:host=localhost;dbname=guilherme_rosso',"root","");
    $select = $conexao->prepare("SELECT * FROM pedidos");
    $select->execute();
    $fetch = $select-> fetchAll();
	?>
<br><br>
			<div class="container theme-showcase" role="main">
			<div class="page-header">
		<div class="row">
					<div class="col-sm-6 col-md-6">
						<h1>Relatorio de vendas</h1>
					</div>
					</div>
					</div>
	<div class="row">
				<?php
     foreach ($fetch as $produto){
		 ?>
		 <div class="col-sm-6 col-md-4">
    	   <div class="thumbnail">
			   <div class="caption text-center">
               <h3><?php echo utf8_encode($produto['cod']); ?></h3></a> 
                 <h4><?php echo 'Produto: '. utf8_encode($produto['id_produto']); ?></h4></a>
				 <?php echo '<h4>Quantidade : ' .($produto['quantidade']).'</h4>'; ?></h3></a>
			     <?php  echo '<h4>Preço individual: R$ '.number_format( $produto['preco'],2,",",".") .' Kg </h4>'  ?>
                 <?php  echo '<h4> Total: R$ '.number_format( $produto['total'],2,",",".") .' </h4>'  ?>
		        </div> 
		 </div>
	     </div>
				
				<?php } ?>
			</div>
		<?php } ?>