﻿<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>Estilizando formulários com CSS </title>

</head> 
<body> 
<form name="meuForm" method="post" id="formulario" action="#">
	<div class="box"> 
		<h1>Formulário de Contato :</h1>
 
         <label> 
			<span>Nome Completo</span>
 			<input type="text" class="input_text" name="nome" id="name" required="required"/>
 		</label>
 
		<label>
 			<span>Email</span>
			<input type="email" class="input_text" name="email" id="email"/>
		 </label>
 
		<label>
 			<span>Assunto</span>
			<input type="text" class="input_text" name="assunto" id="subject"/>
		</label>
 
		<label>
			 <span>Mensagem</span>
			<textarea class="messagem" name="mensagem" id="mensagem"></textarea>
			<input type="submit" class="button" value="Enviar" />
		</label>           
	</div>
</form> 
</body>
</html>