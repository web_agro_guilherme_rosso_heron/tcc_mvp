<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<title>Capa</title>
	</head>
	<body>
	<?php
	$conexao= new PDO ('mysql:host=localhost;dbname=guilherme_rosso',"root","");
    $select = $conexao->prepare("SELECT * FROM produtos");
    $select->execute();
    $fetch = $select-> fetchAll();
	?>
	<div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
       <div class="carousel-inner">
          <div class="carousel-item active">
		  <br> <br> <br>
		  	<style>
			.resp{
				margin-left: 100px;
			}  
			@media only screen and (max-width: 600px) {
				.resp{
				margin-left: 20px;
			}	
			}
			</style>
             <img class="d-block w-100 resp" src="imagens/oie_transparent.png" alt="Primeiro Slide">
          </div>
       </div>
    </div>
<div class="container theme-showcase" role="main">
	<div class="page-header">
	   <div class="row">
					<div class="col-md-4">
						<h1>Promoções</h1>
					</div>
					<div class="col-md-6 col-md-offset-2">
						<form class="form-inline" method="GET" action="pesquisar.php">
							<div class="form-group">
								<label for="exampleInputName2">Pesquisar</label>
								<input type="text" name="pesquisar" class="form-control" id="exampleInputName2" placeholder="Digitar...">
								<button type="submit" class="btn btn-primary">Pesquisar</button>
							</div>

						</form>
					</div>
				</div>
	<div class="row">
	<?php
	
     foreach ($fetch as $produto){
		 ?>
		 <div class="col-sm-6 col-md-4">
    	   <div class="thumbnail">
		     <img src=<?php echo "img_produtos/",$produto['foto_prod']?> class="card-img-top"/>
			 <div class="caption text-center">
				<a href="detalhes.php?id=<?php echo $produto['id']; ?>"><h3>
				<?php echo utf8_encode($produto['nome']); ?></h3></a>
				<?php  echo '<h4>R$ '.number_format( $produto['preco'],2,",",".") .' Kg </h4>'  ?>
				<p><a <?php echo 'href="carrinho.php?add=carrinho&id='.$produto['id'].'"';
		?>class="btn btn-primary" role="button">Comprar</a> </p>
		    </div>
		   
		  </div>
		  </div>
<?php 
}
?>	
</div>
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="bootstrap/js/bootstrap.min.js"></script>
	</body>
</html>