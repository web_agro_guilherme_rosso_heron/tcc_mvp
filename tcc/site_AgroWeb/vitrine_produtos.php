
<?php include_once("conexao.php");
//Verificar se está sendo passado na URL a página atual, senao é atribuido a pagina 
$pagina = (isset($_GET['pagina']))? $_GET['pagina'] : 1;
$result= "SELECT * FROM produtos";
$resultado = mysqli_query($conn, $result);
//Contar o total de produtos
$total_produtos= mysqli_num_rows($resultado);

//Seta a quantidade de produtos por pagina
$quantidade_pg = 6;
//calcular o número de pagina necessárias para apresentar os produtos
$num_pagina = ceil($total_produtos/$quantidade_pg);

//Calcular o inicio da visualizacao
$incio = ($quantidade_pg*$pagina)-$quantidade_pg;

//Selecionar os produtos a serem apresentado na página
$result = "SELECT * FROM produtos limit $incio, $quantidade_pg";
$resultado= mysqli_query($conn, $result);
$total_produtos = mysqli_num_rows($resultado);

?>
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Criar vitrine de produtos</title>
		<link href="css/bootstrap.css" rel="stylesheet">
		<link href="css/signin.css" rel="stylesheet">
	</head>
<!--================-->
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#links-menu" style="background-color: white;">
					<span class="navbar-toggler-icon">menu</span>	
				</button>
			</div>
			

			<nav id="links-menu" class="collapse navbar-collapse">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="index.php?id=capa">Home</a></li>
					<li><a href="index.php?id=fale_conosco">Contato</a></li>
					<li><a href="index.php?id=vitrine_produtos">Produtos</a></li>
					<li><a href="index.php?id=intranet">Login</a></li>
					<li><a href="index.php?id=carrinho"><span class="glyphicon glyphicon-shopping-cart"></span></a></li>
					
				</ul>
			</nav>
		</div>
	</nav>
<!--================-->
	<body>
		<div class="container theme-showcase" role="main">
			<div class="page-header">
		<div class="row">
					<div class="col-sm-6 col-md-6">
						<h1>Produtos</h1>
					</div>
					<div class="col-sm-6 col-md-6">
						<form class="form-inline" method="GET" action="pesquisar.php">
							<div class="form-group">
								<label for="exampleInputName2">Pesquisar</label>
								<input type="text" name="pesquisar" class="form-control" id="exampleInputName2" placeholder="Digitar...">
							</div>
							<button type="submit" class="btn btn-primary">Pesquisar</button>
						</form>
					</div>
				</div>
			</div>
			<?php
	$conexao= new PDO ('mysql:host=localhost;dbname=guilherme_rosso',"root","");
    $select = $conexao->prepare("SELECT * FROM produtos");
    $select->execute();
    $fetch = $select-> fetchAll();
	?>
			<div class="row">
				<?php
     foreach ($fetch as $produto){
		 ?>
		 <div class="col-sm-6 col-md-4">
    	   <div class="thumbnail">
		     <img src=<?php echo "img_produtos/",$produto['foto_prod']?> class="card-img-top"/>
			   <div class="caption text-center">
			     <a href="detalhes.php?id=<?php echo $produto['id']; ?>"><h3>
			     <?php echo utf8_encode($produto['nome']); ?></h3></a>
			     <?php  echo '<h4>R$ '.number_format( $produto['preco'],2,",",".") .' Kg </h4>'  ?>
			     <p><a <?php echo 'href="carrinho.php?add=carrinho&id='.$produto['id'].'"';
	             ?>class="btn btn-primary" role="button">Comprar</a> </p>
		        </div> 
		 </div>
	     </div>
				
				<?php } ?>
			</div>
			
			<?php
				//Verificar a pagina anterior e posterior
				$pagina_anterior = $pagina - 1;
				$pagina_posterior = $pagina + 1;
			?>
			<nav class="text-center">
				<ul class="pagination">
					<li>
						<?php
						if($pagina_anterior != 0){ ?>
							<a href="vitrine_produtos.php?pagina=<?php echo $pagina_anterior; ?>" aria-label="Previous">
								<span aria-hidden="true">&laquo;</span>
							</a>
						<?php }else{ ?>
							<span aria-hidden="true">&laquo;</span>
					<?php }  ?>
					</li>
					<?php 
					//Apresentar a paginacao
					for($i = 1; $i < $num_pagina + 1; $i++){ ?>
						<li><a href="vitrine_produtos.php?pagina=<?php echo $i; ?>"><?php echo $i; ?></a></li>
					<?php } ?>
					<li>
						<?php
						if($pagina_posterior <= $num_pagina){ ?>
							<a href="vitrine_produtos.php?pagina=<?php echo $pagina_posterior; ?>" aria-label="Previous">
								<span aria-hidden="true">&raquo;</span>
							</a>
						<?php }else{ ?>
							<span aria-hidden="true">&raquo;</span>
					<?php }  ?>
					</li>
				</ul>
			</nav>
		</div>
		
		
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="bootstrap/js/bootstrap.min.js"></script>
	</body>
</html>