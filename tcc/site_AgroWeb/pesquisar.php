<?php include_once("conexao.php");
//Verificar se está sendo passado na URL a página atual, senao é atribuido a pagina 
$pagina = (isset($_GET['pagina']))? $_GET['pagina'] : 1;
if(!isset($_GET['pesquisar'])){
	header("Location: vitrine_produtos.php");
}else{
	$valor_pesquisar = $_GET['pesquisar'];
}


//Selecionar todos os produ da tabela
$result = "SELECT * FROM produtos WHERE nome LIKE '%$valor_pesquisar%'";
$resultado = mysqli_query($conn, $result);

//Contar o total de produtos
$total = mysqli_num_rows($resultado);

//Seta a quantidade de produtos por pagina
$quantidade_pg = 6;

//calcular o número de pagina necessárias para apresentar os produtos
$num_pagina = ceil($total/$quantidade_pg);

//Calcular o inicio da visualizacao
$incio = ($quantidade_pg*$pagina)-$quantidade_pg;

//Selecionar os produtos a serem apresentado na página
$result= "SELECT * FROM produtos WHERE nome LIKE '%$valor_pesquisar%' limit $incio, $quantidade_pg";
$resultado = mysqli_query($conn, $result);
$total= mysqli_num_rows($resultado);
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
    <meta http-equiv=”content-type” content="text/html;" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="keywords" content="tags, que, eu, quiser, usar, para, os, robos, do, google" />
    <title> AgroWeb</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<!-- BOOTSTRAP -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
        
		<!-- ESTILOS PARA ESTA PÁGINA -->
		<!-- Nesse caso, este estilo é apenas para inserir imagens -->
		<link rel="shortcut icon" href="imagens/favicon.png" type="image/x-icon" />
		<!-- JAVASCRIPT E JQUERY -->
 <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>

</head>
<script src="bootstrap/js/jquery-3.3.1.min.js"></script>
<body>
</head>
<!-- Inicio do menu superior -->
<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#links-menu" style="background-color: white;">
					<span class="navbar-toggler-icon">menu</span>	
				</button>
			</div>
			

			<nav id="links-menu" class="collapse navbar-collapse">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="index.php?id=capa">Home</a></li>
					<li><a href="index.php?id=fale_conosco">Contato</a></li>
					<li><a href="index.php?id=vitrine_produtos">Produtos</a></li>
					<li><a href="index.php?id=intranet">Login</a></li>
					<li><a href="index.php?id=carrinho"><span class="glyphicon glyphicon-shopping-cart"></span></a></li>
					
				</ul>
			</nav>
		</div>
	</nav>

<html lang="pt-br">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Criar pagina com abas</title>
		<link href="css/bootstrap.min.css" rel="stylesheet">
	</head>
	<body>
		<div class="container theme-showcase" role="main">
			<div class="page-header">
				<div class="row">
					<div class="col-sm-6 col-md-6">
						<h1>Produtos</h1>
					</div>
					<br>
					<div class="col-sm-6 col-md-6">
						<form class="form-inline" method="GET" action="pesquisar.php">
							<div class="form-group">
								<label for="exampleInputName2">Pesquisar</label>
								<input type="text" name="pesquisar" class="form-control" id="exampleInputName2" placeholder="Digitar...">
							</div>
							<button type="submit" class="btn btn-primary">Pesquisar</button>
						</form>
					</div>
				</div>
			</div>
			<div class="row">
				<?php while($rows_produtos = mysqli_fetch_assoc($resultado)){ ?>
					<div class="col-sm-6 col-md-4">
						<div class="thumbnail">
							<img src=<?php echo "img_produtos/",$rows_produtos['foto_prod'] ?> class="card-img-top"/>
							<div class="caption text-center">
								<a href="detalhes.php?id=<?php echo $rows_produtos['id']; ?>">
								<h3><?php echo $rows_produtos['nome']; ?></h3></a>
							<p><h4><?php echo 'R$ ' . $rows_produtos['quantidade'] . 'Kg'; ?></p>
								
							<p><a <?php echo 'href="carrinho.php?add=carrinho&id='.$rows_produtos['id'].'"';
	             ?>class="btn btn-primary" role="button">Comprar</a> </p>
		        
							</div>
						</div>
					</div>
				<?php } ?>
			</div>
			<?php
				//Verificar a pagina anterior e posterior
				$pagina_anterior = $pagina - 1;
				$pagina_posterior = $pagina + 1;
			?>
			<nav class="text-center">
				<ul class="pagination">
					<li>
						<?php
						if($pagina_anterior != 0){ ?>
							<a href="pesquisar.php?pagina=<?php echo $pagina_anterior; ?>&pesquisar=<?php echo $valor_pesquisar; ?>" aria-label="Previous">
								<span aria-hidden="true">&laquo;</span>
							</a>
						<?php }else{ ?>
							<span aria-hidden="true">&laquo;</span>
					<?php }  ?>
					</li>
					<?php 
					//Apresentar a paginacao
					for($i = 1; $i < $num_pagina + 1; $i++){ ?>
						<li><a href="pesquisar.php?pagina=<?php echo $i; ?>&pesquisar=<?php echo $valor_pesquisar; ?>"><?php echo $i; ?></a></li>
					<?php } ?>
					<li>
						<?php
						if($pagina_posterior <= $num_pagina){ ?>
							<a href="pesquisar.php?pagina=<?php echo $pagina_posterior; ?>&pesquisar=<?php echo $valor_pesquisar; ?>" aria-label="Previous">
								<span aria-hidden="true">&raquo;</span>
							</a>
						<?php }else{ ?>
							<span aria-hidden="true">&raquo;</span>
					<?php }  ?>
					</li>
				</ul>
			</nav>
		</div>
		
		
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="js/bootstrap.min.js"></script>
	</body>
</html>