<!doctype html>
<html>
<head>
<meta charset="utf-8">
    <meta http-equiv=”content-type” content="text/html;" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="keywords" content="tags, que, eu, quiser, usar, para, os, robos, do, google" />
    <title> AgroWeb</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<!-- BOOTSTRAP -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
        
		<!-- ESTILOS PARA ESTA PÁGINA -->
		<!-- Nesse caso, este estilo é apenas para inserir imagens -->
		<link rel="shortcut icon" href="imagens/favicon.png" type="image/x-icon" />
		<!-- JAVASCRIPT E JQUERY -->
 <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>

</head>
<script src="bootstrap/js/jquery-3.3.1.min.js"></script>
<body>
</head>
<!-- Inicio do menu superior -->
<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#links-menu" style="background-color: white;">
					<span class="navbar-toggler-icon">menu</span>	
				</button>
			</div>
			

			<nav id="links-menu" class="collapse navbar-collapse">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="index.php?id=capa">Home</a></li>
					<li><a href="index.php?id=fale_conosco">Contato</a></li>
					<li><a href="index.php?id=vitrine_produtos">Produtos</a></li>
					<li><a href="index.php?id=intranet">Login</a></li>
					<li><a href="index.php?id=carrinho"><span class="glyphicon glyphicon-shopping-cart"></span></a></li>
					
				</ul>
			</nav>
		</div>
	</nav>
<?php
if(!isset($_GET['id'])){
	$id="capa";
	include("$id.php");
}else {
	$id=$_GET['id'];
	if(file_exists("$id.php")){
     include("$id.php");
	}else {
	 include("404.php");
	}
}	
	?> 
</body>
</html>
