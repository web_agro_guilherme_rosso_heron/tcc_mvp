-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 29-Nov-2019 às 01:57
-- Versão do servidor: 5.6.15-log
-- PHP Version: 5.4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `guilherme_rosso`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `mensagens`
--

CREATE TABLE IF NOT EXISTS `mensagens` (
  `cod` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `email` varchar(60) NOT NULL,
  `fone` varchar(30) NOT NULL,
  `mensagem` longtext NOT NULL,
  `resposta` char(2) NOT NULL DEFAULT 'n',
  `lido` char(2) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`cod`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Extraindo dados da tabela `mensagens`
--

INSERT INTO `mensagens` (`cod`, `nome`, `email`, `fone`, `mensagem`, `resposta`, `lido`) VALUES
(1, 'gui', 'guicarrosso@gmail.com', '9882525255', 'texti texti\r\ntexti', 'n', 's'),
(3, 'nfjbfeb', 'rbfuvrgfu@gmail.com', 'rnjrdhvur', 'fneufui\r\n', 'n', 's'),
(4, 'gui', 'leandrocardoso2@hotmail.com', '9882525255', 'defgedfd', 'n', 's'),
(5, 'paulo', 'Guixaxxx@gmail.com', '112211212', '1212121212121212', 'n', 's'),
(6, 'gui', 'guilherme@gmail.com', '988403097', 'ola meu consagrado tudo bem como faz pra comprar aqui\r\n', 'n', 'n'),
(8, 'aaa', 'aaaa@gmail.com', 'aaa', 'aaaaaaaaaaaa', 'n', 's'),
(9, 'fernanda', 'fernanda@gmail.com', '988888888', 'ola tudo bem ', 'n', 'n');

-- --------------------------------------------------------

--
-- Estrutura da tabela `pedidos`
--

CREATE TABLE IF NOT EXISTS `pedidos` (
  `id_pedido` int(11) NOT NULL AUTO_INCREMENT,
  `id_produto` int(11) NOT NULL,
  `quantidade` int(11) NOT NULL,
  `preco` float NOT NULL,
  `total` float NOT NULL,
  `cod` varchar(50) NOT NULL,
  PRIMARY KEY (`id_pedido`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=67 ;

--
-- Extraindo dados da tabela `pedidos`
--

INSERT INTO `pedidos` (`id_pedido`, `id_produto`, `quantidade`, `preco`, `total`, `cod`) VALUES
(59, 2, 1, 20, 20, 'heron'),
(60, 1, 1, 15, 15, 'Vinicius '),
(62, 1, 1, 15, 15, 'Guilherme Rosso Cardoso');

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos`
--

CREATE TABLE IF NOT EXISTS `produtos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `quantidade` varchar(10) NOT NULL,
  `preco` decimal(10,2) NOT NULL,
  `descricao` longtext NOT NULL,
  `foto_prod` varchar(50) NOT NULL,
  `categ_prod` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Extraindo dados da tabela `produtos`
--

INSERT INTO `produtos` (`id`, `nome`, `quantidade`, `preco`, `descricao`, `foto_prod`, `categ_prod`) VALUES
(1, 'acem', '20', '15.00', 'Carne com somente 5% de teor de gordura e é ideal para o dia a dia. Congelada individualmente e embalada em pacotes de 1kg com sistema abre fácil, é perfeita para uma refeição nutritiva e saborosa em família, com muita praticidade.', 'acem.jpg', 'carne'),
(2, 'alcatra completa', '1', '20.00', 'Carne com somente 5% de teor de gordura e é ideal para o dia a dia. Congelada individualmente e embalada em pacotes de 1kg com sistema abre fácil, é perfeita para uma refeição nutritiva e saborosa em família, com muita praticidade', 'alcatra_completa.jpg', 'carne'),
(3, 'alcatra miolo', '1', '20.00', 'Carne com somente 5% de teor de gordura e é ideal para o dia a dia. Congelada individualmente e embalada em pacotes de 1kg com sistema abre fácil, é perfeita para uma refeição nutritiva e saborosa em família, com muita praticidade', 'alcatra_miolo.jpg', 'carne'),
(4, 'costela', '1', '24.00', 'Carne com somente 5% de teor de gordura e é ideal para o dia a dia. Congelada individualmente e embalada em pacotes de 1kg com sistema abre fácil, é perfeita para uma refeição nutritiva e saborosa em família, com muita praticidade', 'costela.jpg', 'carne'),
(5, 'costela com file', '1', '20.00', 'Carne com somente 5% de teor de gordura e é ideal para o dia a dia. Congelada individualmente e embalada em pacotes de 1kg com sistema abre fácil, é perfeita para uma refeição nutritiva e saborosa em família, com muita praticidade', 'costela_com_file.jpg', 'carne'),
(6, 'coxão de fora', '15', '15.00', 'Carne com somente 5% de teor de gordura e é ideal para o dia a dia. Congelada individualmente e embalada em pacotes de 1kg com sistema abre fácil, é perfeita para uma refeição nutritiva e saborosa em família, com muita praticidade', 'coxao_fora.jpg', 'carne'),
(7, 'coxão mole', '5', '20.00', 'Carne com somente 5% de teor de gordura e é ideal para o dia a dia. Congelada individualmente e embalada em pacotes de 1kg com sistema abre fácil, é perfeita para uma refeição nutritiva e saborosa em família, com muita praticidade', 'coxao_mole.jpg', 'carne'),
(11, 'FILÉ MIGNON C/ CORDÃO', '100', '29.00', 'Carne com somente 5% de teor de gordura e é ideal para o dia a dia. Congelada individualmente e embalada em pacotes de 1kg com sistema abre fácil, é perfeita para uma refeição nutritiva e saborosa em família, com muita praticidade', 'file_mignon_com_cordao.jpg', 'carne'),
(12, 'lagarto', '50', '23.00', 'Carne com somente 5% de teor de gordura e é ideal para o dia a dia. Congelada individualmente e embalada em pacotes de 1kg com sistema abre fácil, é perfeita para uma refeição nutritiva e saborosa em família, com muita praticidade', 'lagarto.jpg', 'carne'),
(13, 'músculo mole', '510', '12.00', 'Carne com somente 5% de teor de gordura e é ideal para o dia a dia. Congelada individualmente e embalada em pacotes de 1kg com sistema abre fácil, é perfeita para uma refeição nutritiva e saborosa em família, com muita praticidade', 'musculo_mole.jpg', 'carne'),
(14, 'Paleta S/Lagarto', '200', '14.00', 'Carne com somente 5% de teor de gordura e é ideal para o dia a dia. Congelada individualmente e embalada em pacotes de 1kg com sistema abre fácil, é perfeita para uma refeição nutritiva e saborosa em família, com muita praticidade', 'paleta.jpg', 'carne'),
(15, 'patinho', '45', '16.00', 'Carne com somente 5% de teor de gordura e é ideal para o dia a dia. Congelada individualmente e embalada em pacotes de 1kg com sistema abre fácil, é perfeita para uma refeição nutritiva e saborosa em família, com muita praticidade', 'patinho.jpg', 'carne');

-- --------------------------------------------------------

--
-- Estrutura da tabela `relatorio`
--

CREATE TABLE IF NOT EXISTS `relatorio` (
  `codigo` int(11) NOT NULL AUTO_INCREMENT,
  `produto` varchar(70) NOT NULL,
  `fornecedor` varchar(70) NOT NULL,
  `datapg` date NOT NULL,
  `datavc` date NOT NULL,
  `situacao` varchar(100) NOT NULL,
  `valor` decimal(9,2) DEFAULT NULL,
  PRIMARY KEY (`codigo`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `cod` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `login` varchar(30) NOT NULL,
  `senha` varchar(30) NOT NULL,
  `priv` int(11) NOT NULL,
  `fone` varchar(30) NOT NULL,
  PRIMARY KEY (`cod`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=48 ;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`cod`, `nome`, `login`, `senha`, `priv`, `fone`) VALUES
(1, 'Guilherme Rosso Cardoso', 'gui', '12345', 7, '48999999999'),
(29, 'Heron Gustavo', 'heron', '12345', 7, '48999999999'),
(35, 'joao', 'joao', '1245', 6, '48999999999'),
(45, 'Vinicius ', 'VineDRC', '12345', 5, '48999999999');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
