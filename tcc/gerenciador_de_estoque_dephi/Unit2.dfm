object Flogin: TFlogin
  Left = 722
  Top = 152
  BorderStyle = bsNone
  Caption = 'Login'
  ClientHeight = 292
  ClientWidth = 411
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 136
    Top = 24
    Width = 72
    Height = 24
    Caption = 'Usuario'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object Label2: TLabel
    Left = 136
    Top = 88
    Width = 61
    Height = 24
    Caption = 'Senha'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
  end
  object SpeedButton1: TSpeedButton
    Left = 144
    Top = 184
    Width = 129
    Height = 30
    Cursor = crHandPoint
    Caption = 'Entrar'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    OnClick = SpeedButton1Click
  end
  object SpeedButton3: TSpeedButton
    Left = 144
    Top = 224
    Width = 129
    Height = 30
    Cursor = crHandPoint
    Caption = 'Sair'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    OnClick = SpeedButton3Click
  end
  object Edit1: TEdit
    Left = 136
    Top = 56
    Width = 145
    Height = 21
    Cursor = crIBeam
    TabOrder = 0
  end
  object Edit2: TEdit
    Left = 136
    Top = 120
    Width = 145
    Height = 21
    Cursor = crIBeam
    PasswordChar = '*'
    TabOrder = 1
  end
  object MainMenu1: TMainMenu
    Left = 336
    Top = 144
    object Ajuda1: TMenuItem
      Caption = 'Ajuda'
      OnClick = Ajuda1Click
    end
  end
end
