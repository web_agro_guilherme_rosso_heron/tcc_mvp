object Fpesquisa: TFpesquisa
  Left = 0
  Top = 0
  Caption = 'Pesquisa'
  ClientHeight = 217
  ClientWidth = 506
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Button3: TButton
    Left = 400
    Top = 8
    Width = 97
    Height = 26
    Caption = 'Pesquisa'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 0
    OnClick = Button3Click
  end
  object Edit1: TEdit
    Left = 8
    Top = 8
    Width = 266
    Height = 26
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
  end
  object DBGrid1: TDBGrid
    Left = 8
    Top = 40
    Width = 489
    Height = 137
    DataSource = Banco.DataSource1
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'codigo'
        Width = 45
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'produto'
        Width = 74
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'fornecedor'
        Width = 98
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'datapg'
        Width = 71
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'datavc'
        Width = 69
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'valor'
        Width = 55
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'situacao'
        Width = 104
        Visible = True
      end>
  end
  object Button1: TButton
    Left = 8
    Top = 187
    Width = 75
    Height = 25
    Caption = 'Voltar'
    TabOrder = 3
    OnClick = Button1Click
  end
end
