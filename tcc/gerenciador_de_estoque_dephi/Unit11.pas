unit Unit11;

interface

uses
  SysUtils, Classes, DB, FMTBcd, DBClient, Provider,
  SqlExpr, Data.DBXMySql, FireDAC.Phys.MySQLDef, FireDAC.Stan.Intf,
  FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys,
  FireDAC.Phys.MySQL, FireDAC.VCLUI.Wait, FireDAC.Stan.Param, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.DApt, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TBanco = class(TDataModule)
    FDPhysMySQLDriverLink1: TFDPhysMySQLDriverLink;
    FDConnection1: TFDConnection;
    FDQuery1: TFDQuery;
    DataSource1: TDataSource;
    FDQuery1codigo: TIntegerField;
    FDQuery1produto: TStringField;
    FDQuery1fornecedor: TStringField;
    FDQuery1datapg: TDateField;
    FDQuery1datavc: TDateField;
    FDQuery1situacao: TStringField;
    FDQuery1valor: TBCDField;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Banco: TBanco;

implementation

{$R *.dfm}

procedure TBanco.DataModuleCreate(Sender: TObject);
begin
Banco.FDQuery1.Open;
end;

end.
