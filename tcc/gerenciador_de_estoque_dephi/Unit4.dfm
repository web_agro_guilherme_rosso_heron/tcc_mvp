object Fconsulta: TFconsulta
  Left = 618
  Top = 161
  BorderStyle = bsNone
  Caption = 'Consulta'
  ClientHeight = 408
  ClientWidth = 651
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 16
    Top = 93
    Width = 37
    Height = 13
    Caption = 'Produto'
  end
  object Label3: TLabel
    Left = 161
    Top = 96
    Width = 54
    Height = 13
    Caption = 'Fornecedor'
  end
  object Label4: TLabel
    Left = 304
    Top = 96
    Width = 95
    Height = 13
    Caption = 'Data de Pagamento'
  end
  object Label5: TLabel
    Left = 16
    Top = 157
    Width = 97
    Height = 13
    Caption = 'Data de Vencimento'
  end
  object Label6: TLabel
    Left = 160
    Top = 157
    Width = 24
    Height = 13
    Caption = 'Valor'
  end
  object Label7: TLabel
    Left = 305
    Top = 157
    Width = 42
    Height = 13
    Caption = 'Situa'#231#227'o'
  end
  object Label8: TLabel
    Left = 208
    Top = 8
    Width = 255
    Height = 29
    Caption = 'Cadastro de Produtos'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -24
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object DBGrid1: TDBGrid
    Left = 8
    Top = 218
    Width = 521
    Height = 169
    DataSource = Banco.DataSource1
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'produto'
        Width = 78
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'fornecedor'
        Width = 89
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'datapg'
        Width = 69
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'datavc'
        Width = 67
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'valor'
        Width = 56
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'situacao'
        Width = 99
        Visible = True
      end>
  end
  object DBEdit2: TDBEdit
    Left = 16
    Top = 112
    Width = 121
    Height = 21
    DataField = 'produto'
    DataSource = Banco.DataSource1
    TabOrder = 1
  end
  object DBEdit3: TDBEdit
    Left = 161
    Top = 112
    Width = 121
    Height = 21
    DataField = 'fornecedor'
    DataSource = Banco.DataSource1
    TabOrder = 2
  end
  object DBEdit4: TDBEdit
    Left = 304
    Top = 115
    Width = 121
    Height = 21
    DataField = 'datapg'
    DataSource = Banco.DataSource1
    Enabled = False
    TabOrder = 3
  end
  object DBEdit5: TDBEdit
    Left = 16
    Top = 176
    Width = 121
    Height = 21
    DataField = 'datavc'
    DataSource = Banco.DataSource1
    TabOrder = 4
  end
  object DBEdit6: TDBEdit
    Left = 160
    Top = 176
    Width = 121
    Height = 21
    DataField = 'valor'
    DataSource = Banco.DataSource1
    TabOrder = 5
  end
  object DBEdit7: TDBEdit
    Left = 305
    Top = 176
    Width = 121
    Height = 21
    DataField = 'situacao'
    DataSource = Banco.DataSource1
    TabOrder = 6
  end
  object Button1: TButton
    Left = 548
    Top = 216
    Width = 75
    Height = 25
    Caption = 'Adicionar'
    TabOrder = 7
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 548
    Top = 263
    Width = 75
    Height = 25
    Caption = 'Deletar'
    TabOrder = 8
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 548
    Top = 360
    Width = 79
    Height = 25
    Caption = 'Voltar'
    TabOrder = 9
    OnClick = Button3Click
  end
  object Button5: TButton
    Left = 228
    Top = 58
    Width = 101
    Height = 23
    Caption = 'Pesquisa'
    TabOrder = 10
    OnClick = Button5Click
  end
  object Edit1: TEdit
    Left = 16
    Top = 59
    Width = 206
    Height = 21
    TabOrder = 11
  end
  object Button4: TButton
    Left = 548
    Top = 312
    Width = 79
    Height = 25
    Caption = 'Limpar'
    TabOrder = 12
    OnClick = Button4Click
  end
  object Timer1: TTimer
    OnTimer = Timer1Timer
    Left = 512
    Top = 48
  end
end
