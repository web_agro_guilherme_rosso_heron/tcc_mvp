program Project1;

uses
  Forms,
  Unit1 in 'Unit1.pas' {Fsplash},
  Unit2 in 'Unit2.pas' {Flogin},
  Unit3 in 'Unit3.pas' {Fprincipal},
  Unit4 in 'Unit4.pas' {Fconsulta},
  Unit9 in 'Unit9.pas' {Fsair},
  Unit10 in 'Unit10.pas' {Fajuda},
  Unit11 in 'Unit11.pas' {Banco: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TFsplash, Fsplash);
  Application.CreateForm(TFlogin, Flogin);
  Application.CreateForm(TFprincipal, Fprincipal);
  Application.CreateForm(TFconsulta, Fconsulta);
  Application.CreateForm(TFsair, Fsair);
  Application.CreateForm(TFajuda, Fajuda);
  Application.CreateForm(TBanco, Banco);
  Application.Run;
end.
